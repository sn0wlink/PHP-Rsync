# PHP-Rsync

## Description
A small php 'interface' for the linux rSync program to backup files
to a remote server.
    
## REQUIREMENTS
For this to work you must have a Debian / Ubuntu server SSH Pass and R sync 
installed to automatically login to the remote server.