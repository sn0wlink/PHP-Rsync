<html>

<!--
// ================================================================================ //
//                                Documentation                                     //
// ================================================================================ //
    
    
    
    =| DETAILS |=
    Author: David Collins-Cubitt
    Website: http://sn0wlink.com
    Licence: GPLv3
    Description: A small php 'interface' for the linux rSync program to backup files
    to a remote server.
    
    =| REQUIREMENTS |=
    For this to work you must have a Debian / Ubuntu server SSH Pass and R sync 
    installed to automatically login to the remote server.
    
    =| Future Support |=
    * Enable run as a background process to allow the client browser to be closed.
    
    * Future support for Windows boxes may be available soon. Who knows, pigs may 
      indeed fly to mars.
    
// ================================================================================ //
// ================================================================================ //

!-->

<title>=| Freds File Sync |=</title>

<body>
<center>

<!-- Header Text !-->
<h1>Freds File Sync</h1>

<!-- Display Button !-->
<form class="controls" action="filesync.php" method="post">
<input type="submit" name="filesync" value="- SYNC -"/>
</form>

<?php

// ================================================================================ //
//                                SETTINGS - SETUP !                                //
// ================================================================================ //

    // Login Settings
    $serv='ftp.testserver.com';   // SSH Server Address
    $usr='username';              // SSH Username
    $pass='Password';             // SSH Password

    // File Locations
    $from='/var/www/html/test';   // Original files
    $to='/var/www/html/test';     // Copy to location

    // Special Options
    $dispcmd=TRUE;                // display cmdline errors
    $delete=FALSE;                // enable file deletion on remote side
    $force=TRUE;                  // Force yes/no


// ================================================================================ //
//                            WHERE THE MAGIC HAPPENS !                             //
// ================================================================================ //

// Remove no variable notice.
$del = '';
$frc = '';

// Setup Options for Rsync

    if ($delete) {$del='--delete';}
    if ($force) {$frc='--force';}
    

// Runs Rsync with defined commands
    
    if (isset($_POST['filesync'])) {
        
        $exec = shell_exec("sshpass -p $pass rsync $del $frc -rPz $from $usr@$serv:$to");
        
        if (strpos($exec, 'sending incremental file list') !== FALSE && $dispcmd) {
            // Displays Your Success :)
            echo "<p>Your files have been synced</p> <h3>Command Line Output</h3>$exec";
        }
        
        elseif ($dispcmd) {
            // Displays Your Failures :(
            echo "=| Failed |=<br /> $exec";
        }
            
    }
	
?>

</center>
</body>
</html>